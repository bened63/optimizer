/*
*
* (c) 2014 Airbus Defence and Space
*
****************************************************************************
*
* Responsible Persons: Jakob Bauer
*
****************************************************************************
*
*/
#include "OptimizerUnit/CSVItemModel.h"

#include <QStandardItemModel>



void toStdVariantVector(const QStringList& in, CSVItemModel::Row& out) {
	CSVItemModel::Row::size_type i = 0;
	out.resize(in.size());
	for (QStringList::const_iterator itr = in.begin(); itr != in.end(); ++itr) {		
		out[i] = (*itr).trimmed();
		++i;
	}
}

QAbstractItemModel* CSVItemModel::create(const QString& fp, QObject* parent) {
#if 1
	CSVItemModel* m = new CSVItemModel();
	if (!m->append(fp, true, false)) {
		delete m;
		return nullptr;
	}

	m->setParent(parent);
	return m;
#else
	QFile f(fp);
	if (!f.open(QIODevice::ReadOnly)) {
		return nullptr;
	}

	QString headerData = f.readLine();
	QStringList headerNames = headerData.split(',');

	QStandardItemModel* model = new QStandardItemModel(0, headerNames.size(), parent);
	model->setHorizontalHeaderLabels(headerNames);

	while (!f.atEnd()) {
		QString raw = f.readLine();
		QStringList data = raw.split(',');

		QList<QStandardItem*> items;
		for (int i = 0; i < data.size(); ++i) {
			items << new QStandardItem(data.at(i));
		}

		model->appendRow(items);
	}

	return model;
#endif
}

CSVItemModel::CSVItemModel(QObject* parent)
	: QAbstractTableModel(parent)
{
}

CSVItemModel::~CSVItemModel() {
}

int CSVItemModel::rowCount(const QModelIndex &parent) const {
	if (parent.isValid()) {
		return 0;
	}

	return _data.size();
}

int CSVItemModel::columnCount(const QModelIndex &parent) const {
	if (parent.isValid()) {
		return 0;
	}

	if (_data.empty()) {
		if (!_header.empty()) {
			return _header.size();
		}
	}
	else {
		return _data.at(0).size();
	}

	return 0;
}

QVariant CSVItemModel::data(const QModelIndex &index, int role) const {
	if (!index.isValid() || (int)_data.size() <= index.row()) {
		return QVariant();
	}

	if (role == Qt::DecorationRole) {
		if (index.row() < (int)_decoration.size()) {
			return _decoration[index.row()];
		}
	}

	if ((role != Qt::DisplayRole && role != Qt::EditRole)) {
		return QVariant();
	}

	const Row& columns = _data[index.row()];
	if ((int)columns.size() <= index.column()) {
		return QVariant();
	}

	return columns[index.column()];
}

bool CSVItemModel::setData(const QModelIndex &index, const QVariant &value, int role) {
	if (!index.isValid() || (int)_data.size() <= index.row()) {
		return false;
	}

	if (role == Qt::DecorationRole) {
		_decoration[index.row()] = value;
	}

	if (role != Qt::EditRole) {
		return false;
	}

	Row& columns = _data[index.row()];
	if ((int)columns.size() <= index.column()) {
		return false;
	}

	columns[index.column()] = value;
	emit dataChanged(index, index);
	return true;
}

QVariant CSVItemModel::headerData(int section, Qt::Orientation orientation, int role) const {
	if (orientation != Qt::Horizontal || _header.empty() || section >= (int)_header.size()) {
		return QVariant();
	}

	return _header[section];
}

bool CSVItemModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role) {
	if (orientation != Qt::Horizontal || _header.empty() || section >= (int)_header.size()) {
		return false;
	}

	_header[section] = value;
	return true;
}

bool CSVItemModel::insertColumns(int column, int count, const QModelIndex &parent) {
	if (column > columnCount() || parent.isValid()) {
		return false;
	}
	beginInsertColumns(parent, column, column + count - 1);
	_header.insert(_header.begin() + column, count, QVariant());
	for (Data::size_type i = 0; i < _data.size(); ++i) {
		_data[i].insert(_data[i].begin() + column, count, QVariant());
	}
	endInsertColumns();
	return true;
}

bool CSVItemModel::insertRows(int row, int count, const QModelIndex &parent) {
	if ((Data::size_type)row >= _data.size() || count < 1 || parent.isValid()) {
		return false;
	}
	beginInsertRows(parent, row, row + count - 1);
	_data.insert(_data.begin() + row, count, Row(columnCount()));
	_decoration.insert(_decoration.begin() + row, count, QVariant());
	endInsertRows();
	return true;
}

bool CSVItemModel::removeRows(int row, int count, const QModelIndex &parent) {
	if ((Data::size_type)row >= _data.size() || count < 1 || parent.isValid()) {
		return false;
	}
	beginRemoveRows(parent, row, row + count - 1);
	_data.erase(_data.begin() + row, _data.begin() + row + count);
	_decoration.erase(_decoration.begin() + row, _decoration.begin() + row + count);
	endRemoveRows();
	return true;
}

bool CSVItemModel::readData(const QString& fp, bool headerExists, Data& data) {
	QFile f(fp);
	if (!f.open(QIODevice::ReadOnly)) {
		return false;
	}

	if (headerExists) {
		f.readLine();
	}

	data.clear();
	data.reserve(124);
	while (!f.atEnd()) {
		QString raw = QString::fromLatin1(f.readLine());
		if (raw.isEmpty()) {
			continue;
		}

		Row row;
		toStdVariantVector(raw.split(QChar::fromLatin1(',')), row);

		data.push_back(Row());
		data.rbegin()->swap(row);
	}

	return !data.empty();
}

bool CSVItemModel::append(const QString& fp, bool headerExists, bool compareHeaders) {
	QFile f(fp);
	if (!f.open(QIODevice::ReadOnly)) {
		return false;
	}

	if (headerExists) {
		// if header exists, read first line
		QString headerData = QString::fromLatin1(f.readLine());
		QStringList headerNames = headerData.split(QChar::fromLatin1(','));
		Row newHeader;
		toStdVariantVector(headerNames, newHeader);

		if (_header.empty()) {
			_header.swap(newHeader);
		}
		else if (compareHeaders && newHeader != _header) {
			return false; // no equal headers
		}
	}

	int column = columnCount();
	Data data;
	data.reserve(124);
	while (!f.atEnd()) {
		QString raw = QString::fromLatin1(f.readLine());
		if (raw.isEmpty()) {
			continue;
		}

		Row row;
		toStdVariantVector(raw.split(QChar::fromLatin1(',')), row);

		data.push_back(Row());
		data.rbegin()->swap(row);
	}

	return append(data);
}

bool CSVItemModel::append(Data& data) {
	if (data.empty()) {
		return false;
	}

	int column = columnCount();
	beginInsertRows(QModelIndex(), _data.size(), _data.size() + data.size() - 1);
	int oldSize = _data.size();
	Data::size_type offset = _data.size();
	_data.resize(_data.size() + data.size());
	for (Data::size_type i = 0; i < data.size(); ++i) {
		data[i].resize(column); // fixed data length
		_data[offset + i].swap(data[i]);
	}

	_decoration.resize(oldSize + data.size());
	endInsertRows();
	return true;
}

bool CSVItemModel::append(const QString& fp, bool headerExists, bool compareHeaders, const QVector<int> *columnsToManipulate, int indexToAdd) {
	QFile f(fp);
	if (!f.open(QIODevice::ReadOnly)) {
		return false;
	}

	if (headerExists) {
		// if header exists, read first line
		QString headerData = QString::fromLatin1(f.readLine());
		QStringList headerNames = headerData.split(QChar::fromLatin1(','));
		Row newHeader;
		toStdVariantVector(headerNames, newHeader);

		if (_header.empty()) {
			_header.swap(newHeader);
		}
		else if (newHeader != _header) {
			if (compareHeaders) {
				return false; // no equal headers
			}
		}
	}

	Data data;
	data.reserve(124);
	while (!f.atEnd()) {
		QString raw = QString::fromLatin1(f.readLine());
		if (raw.isEmpty()) {
			continue;
		}

		Row row;
		toStdVariantVector(raw.split(QChar::fromLatin1(',')), row);

		if (indexToAdd > 0) {
			for (QVector<int>::const_iterator it = columnsToManipulate->begin(); it != columnsToManipulate->end(); ++it) {
				int c = *it;
				QString stValue = row[c].toString() + QStringLiteral(".%1").arg(indexToAdd);
				row[c] = stValue;
			}
		}

		data.push_back(Row());
		data.rbegin()->swap(row);
	}

	if (data.empty()) {
		return false;
	}

	return append(data);
}