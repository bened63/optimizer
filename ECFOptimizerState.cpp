/*
*
* (c) 2016 Airbus Defence and Space
*
****************************************************************************
*
* Responsible Persons: Benjamin Eder / Daniel Kallfass
*
****************************************************************************
*
*/
#pragma once

#include "ECFOptimizerState.h"
#include <fstream>
#include <iomanip>
#include <time.h>
#include <regex>

#include "OptimizerUnit/FunctionMinEvalOp.h"

/**
 * \brief Construct the one and only ECFOptimizerState object.
 *
 * Also register all known algorithms, genotypes and termination operators, create the population and some other elements of the system.
 */
ECFOptimizerState::ECFOptimizerState()
	: State()
{

}

bool ECFOptimizerState::setGenotypeVariables(ParameterVariationStruct& parameters)
{
	_parameterVariations = parameters;
	return true;
}

/**
* generate genotype for framework
*/
bool ECFOptimizerState::generateGenotype()
{
	typedef std::map<std::string, GenotypeP>::iterator gen_iter;

	//
	// NUMBER: generate genotype FloatingPoint for parameter varations of type number
	//
	if (_parameterVariations.numbers.size() != 0)
	{
		for (int i = 0; i < _parameterVariations.numbers.size(); i++)
		{
			if (!generateFloatingPoint(_parameterVariations.numbers[i].min, _parameterVariations.numbers[i].max))
				return false;

			//std::string typeName = "FloatingPoint";

			//gen_iter gen = mGenotypes_.find(typeName);
			//if (gen == mGenotypes_.end()) {
			//	throw std::string("Error: unknown Genotype : ") + typeName;
			//}

			//uint genotypeId = (uint)genotype_.size();
			//gen->second->setGenotypeId(genotypeId);
			//gen->second->registerParameters(state_);
			//setGenotype(gen->second);

			//std::string key = typeName;

			//if (genotypeId > 0)
			//	key = uint2str(genotypeId) + key;

			//// define attributes of FloatingPoints
			//double *lbound = new double;
			//*lbound = _parameterVariations.numbers[i].min;
			//voidP new_value = (voidP)lbound;
			//if (!registry_->modifyEntry(key + ".lbound", new_value)) // modify register entry
			//	return false;

			//double *ubound = new double;
			//*ubound = _parameterVariations.numbers[i].max;
			//new_value = (voidP)ubound;
			//if (!registry_->modifyEntry(key + ".ubound", new_value)) // modify register entry
			//	return false;

			//int *dimensionInt = new int(1);
			////*dimensionInt = 1, // constant dimension
			//new_value = (voidP)dimensionInt;
			//if (!registry_->modifyEntry(key + ".dimension", new_value)) // modify register entry
			//	return false;

		} // end for
	} // end if(numbers.size!=0)


	//
	// ENUMERATION: generate genotype
	//
	if (_parameterVariations.enumerations.size() != 0)
	{
		// set type for enumerations
		std::string useGenType = "FloatingPoint";

		// size of BitString
		uint bitStr_size = 0;

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// const dimension = 1 for every FloatingPoint ////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		for (EnumerationStruct enumeration : _parameterVariations.enumerations)
		{
			// default case
			if (enumeration.type == "enumeration")
			{
				// generate FloatingPoint
				if (useGenType == "FloatingPoint")
				{
					if (!generateFloatingPoint(0, enumeration.enumValues.size() - 0.0001))
						return false;
				}
			}

			// special case for RiKoV
			if (enumeration.type == "vector")
			{
				double minVec[3] = {}; //  minmum (x,y,z)
				double maxVec[3] = {}; // maximum (x,y,z)

				readVectorString(enumeration.enumValues.at(0), minVec); // min
				readVectorString(enumeration.enumValues.at(1), maxVec); // max

				// generate FloatingPoint
				if (useGenType == "FloatingPoint")
				{
					// x value
					if (!generateFloatingPoint(minVec[0], maxVec[0]))
						return false;

					// y value
					if (!generateFloatingPoint(minVec[1], maxVec[1]))
						return false;
				}
			}

			// generate Binary
			//if (useGenType == "Binary")
			//{
			//	std::string typeName = "Binary";

			//	// BitString: count enumerations with two enumValues
			//	if (enumeration.enumValues.size() == 2)
			//	{
			//		bitStr_size++; // increment size of BitString
			//	}

			//	gen_iter gen = mGenotypes_.find(typeName);
			//	if (gen == mGenotypes_.end()) {
			//		throw std::string("Error: unknown Genotype : ") + typeName;
			//	}

			//	uint genotypeId = (uint)genotype_.size();
			//	gen->second->setGenotypeId(genotypeId);
			//	gen->second->registerParameters(state_);
			//	setGenotype(gen->second);

			//	std::string key = typeName;

			//	if (genotypeId > 0)
			//		key = uint2str(genotypeId) + key;

			//	// define attributes of Binary
			//	double *lbound = new double;
			//	*lbound = 0;
			//	voidP new_value = (voidP)lbound;
			//	if (!registry_->modifyEntry(key + ".lbound", new_value)) // modify register entry
			//		return false;

			//	double *ubound = new double;
			//	//*ubound = 1;
			//	*ubound = (enumeration.enumValues.size() - 1);
			//	new_value = (voidP)ubound;
			//	if (!registry_->modifyEntry(key + ".ubound", new_value)) // modify register entry
			//		return false;

			//	uint *precision = new uint;
			//	*precision = 0;
			//	new_value = (voidP)precision;
			//	if (!registry_->modifyEntry(key + ".precision", new_value)) // modify register entry
			//		return false;

			//	uint *rounding = new uint;
			//	*rounding = 1;
			//	new_value = (voidP)rounding;
			//	if (!registry_->modifyEntry(key + ".rounding", new_value)) // modify register entry
			//		return false;

			//	uint *dim = new uint;
			//	*dim = 1;
			//	new_value = (voidP)dim;
			//	if (!registry_->modifyEntry(key + ".dimension", new_value)) // modify register entry
			//		return false;

			//} //end if (Binary)

		} // end for
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// generate BitString
		//if (useGenType == "BitString")
		//{
		//	std::string typeName = "BitString";

		//	gen_iter gen = mGenotypes_.find(typeName);
		//	if (gen == mGenotypes_.end()) {
		//		throw std::string("Error: unknown Genotype : ") + typeName;
		//	}

		//	uint genotypeId = (uint)genotype_.size();
		//	gen->second->setGenotypeId(genotypeId);
		//	gen->second->registerParameters(state_);
		//	setGenotype(gen->second);

		//	std::string key = typeName;

		//	if (genotypeId > 0)
		//		key = uint2str(genotypeId) + key;

		//	uint *size = new uint;
		//	*size = bitStr_size;
		//	voidP new_value = (voidP)size;
		//	if (!registry_->modifyEntry(key + ".size", new_value)) // modify register entry
		//		return false;

		//} // end of (Binary)

	}// end if

	return true;
}

/**
* \brief	Parse given config file.
* Called by: State::initialize
* \param filename	config file name
*/
bool ECFOptimizerState::parseConfig(std::string filename)
{
	// current folder = /simcore_projects/PAXSEM/bin
	std::string filepath = "../../Core/VisualStudio12/OptimizerUnit/" + filename;

	std::ifstream fin(filepath.c_str());
	if (!fin) {
		throw std::string("Error opening file " + filename);
	}
	std::cout << "Parsing configuration file: " << filename << std::endl;

	std::string xmlFile, temp;
	while (!fin.eof()) {
		getline(fin, temp);
		xmlFile += "\n" + temp;
	}

	XMLResults results;
	xConfig_ = XMLNode::parseString(xmlFile.c_str(), "ECF", &results);
	if (results.error != eXMLErrorNone) {
		std::cerr << "Configuration file: " << XMLNode::getError(results.error);
		std::cerr << " (line " << results.nLine << ", col " << results.nColumn << ")" << std::endl;
		throw("");
	}

	if (xConfig_.isEmpty())
		return false;

	int n = xConfig_.nChildNode();
	for (int i = 0; i < n; ++i) {
		XMLNode     child = xConfig_.getChildNode(i);
		std::string name = child.getName();
		bool        ok = true;

		if (name == NODE_REGISTRY)
			ok &= registry_->readEntries(child);
		else if (name == NODE_ALGORITHM)
			ok &= parseAlgorithmNode(child);
		else if (name == NODE_GENOTYPE)
		{
			ok &= generateGenotype(); // NEW: generate genotypes from dfe parameter variations!!!
			//ok &= parseGenotypeNode(child); // read genotypes from parameters.txt (default)
		}
		else if (name == NODE_POPULATION)
			continue;
		else
			std::cerr << "Unknown node: " << name << std::endl;

		if (!ok)
			throw "";
	}

	return true;
}

bool ECFOptimizerState::runInitialize() {
	// command line only (no evolution)
	if (bCommandLine_)
		return false;

	if (!bInitialized_) {
		std::cerr << "Error: Initialization failed!" << std::endl;
		return false;
	}

	if (bBatchStart_) {
		ECF_LOG(this, 5, "Batch mode detected: running batch");
		runBatch();
		return true;
	}

	startTime_ = time(NULL);
	std::string stime = ctime(&startTime_);
	ECF_LOG(this, 3, "Start time: " + stime);
	// adjust with previous runtime (from milestone)
	startTime_ -= milestoneElapsedTime_;

	// 1. run DF experiments for initial population
	////runDfExperiments(); // NEW ##############################################################################################

	// 2. evaluate fitness of population (executes FunctionMinEvalOp::evaluate() function)
	ECF_LOG(this, 2, "Evaluating initial population...");
	algorithm_->initializePopulation(state_);

	currentTime_ = time(NULL);
	elapsedTime_ = currentTime_ - startTime_;
	std::cout << std::endl;
	ECF_LOG(this, 2, "Generation: " + uint2str(context_->generationNo_));
	ECF_LOG(this, 2, "Elapsed time: " + uint2str((uint)elapsedTime_));
	population_->updateDemeStats();

	// call user-defined operators
	ECF_LOG(this, 4, "Calling user defined operators...");
	for (uint i = 0; i < activeUserOps_.size(); i++)
		activeUserOps_[i]->operate(state_);

	// termination ops
	ECF_LOG(this, 4, "Checking termination conditions...");
	for (uint i = 0; i < activeTerminationOps_.size(); i++)
		activeTerminationOps_[i]->operate(state_);

	return true;
}

bool ECFOptimizerState::isRunTerminated() {
	return context_->bTerminate_;
}

//run next generation demes
bool ECFOptimizerState::runNextGeneration() {
	context_->generationNo_++;
	ECF_LOG(this, 5, "Calling the active algorithm");

	// 1. run DF experiments for initial population
	//runDfExperiments(); // NEW ##############################################################################################

	// 2. evaluate fitness of population (executes FunctionMinEvalOp::evaluate() function)
	algorithm_->advanceGeneration(state_);

	currentTime_ = time(NULL);
	elapsedTime_ = currentTime_ - startTime_;
	std::cout << std::endl;
	ECF_LOG(this, 2, "Generation: " + uint2str(context_->generationNo_));
	ECF_LOG(this, 2, "Elapsed time: " + uint2str((uint)elapsedTime_));

	population_->updateDemeStats();

	// call user-defined operators
	ECF_LOG(this, 4, "Calling user defined operators...");
	for (uint i = 0; i < activeUserOps_.size(); i++)
		activeUserOps_[i]->operate(state_);

	// termination ops
	ECF_LOG(this, 4, "Checking termination conditions...");
	for (uint i = 0; i < activeTerminationOps_.size(); i++)
		activeTerminationOps_[i]->operate(state_);

	if (context_->bTerminate_)
		logger_->saveTo(true);
	else
		logger_->saveTo();

	if (bSaveMilestone_ &&
		milestoneInterval_ > 0 && context_->generationNo_ % milestoneInterval_ == 0)
		saveMilestone();

	return migration_->operate(state_);
}

bool ECFOptimizerState::getCurrentPopulation() {
	//DemeP myDeme = getPopulation()->getLocalDeme();
	//uint demeSize = myDeme->getSize();

	for (uint iDeme = 0; iDeme < getPopulation()->size(); iDeme++) {
		int populationSize = getPopulation()->size();
		DemeP deme = getPopulation()->at(iDeme);
		uint demeSize = deme->size();
		for (uint iInd = 0; iInd < demeSize; iInd++) {
			IndividualP individual = deme->at(iInd);
			//TODO: add all demes and its individuals to the Data Farming experiment
		}
	}

	return true;
}


bool ECFOptimizerState::outputHallOfFame() {
	// output HallOfFame
	XMLNode xHoF;
	population_->getHof()->write(xHoF);
	char *out = xHoF.createXMLString(true);
	ECF_LOG(this, 1, "\nElapsed time: " + uint2str((uint)elapsedTime_));
	ECF_LOG(this, 1, "Best of run: \n" + std::string(out));
	freeXMLString(out);

	logger_->saveTo(true);
	if (bSaveMilestone_)
		saveMilestone();

	logger_->closeLog();

	return true;
}

bool ECFOptimizerState::runDfExperiments()
{
	// cast to derived class
	FunctionMinEvalOpP funMinEvalOp_ = dynamic_pointer_cast<FunctionMinEvalOp>(evalOp_);

	// start DF experiment for each individual
	for (uint iDeme = 0; iDeme < getPopulation()->size(); iDeme++)
		for (uint iInd = 0; iInd < getPopulation()->at(iDeme)->size(); iInd++)
		{

			// start DF experiment
			funMinEvalOp_->startDfExperiment(getPopulation()->at(iDeme)->at(iInd));
		}

	// save study ids of current generation
	//funMinEvalOp_->saveGenerationStudyIds();

	return true;
}

bool ECFOptimizerState::generateFloatingPoint(double min, double max, int dim)
{
	std::string typeName = "FloatingPoint";

	gen_iter gen = mGenotypes_.find(typeName);
	if (gen == mGenotypes_.end()) {
		throw std::string("Error: unknown Genotype : ") + typeName;
	}

	uint genotypeId = (uint)genotype_.size();
	gen->second->setGenotypeId(genotypeId);
	gen->second->registerParameters(state_);
	setGenotype(gen->second);

	std::string key = typeName;

	if (genotypeId > 0)
		key = uint2str(genotypeId) + key;

	// define attributes of FloatingPoints
	double *lbound = new double;
	*lbound = min;
	voidP new_value = (voidP)lbound;
	if (!registry_->modifyEntry(key + ".lbound", new_value)) // modify register entry
		return false;

	double *ubound = new double;
	*ubound = max;
	new_value = (voidP)ubound;
	if (!registry_->modifyEntry(key + ".ubound", new_value)) // modify register entry
		return false;

	int *dimensionInt = new int(dim); // constant dimension
	new_value = (voidP)dimensionInt;
	if (!registry_->modifyEntry(key + ".dimension", new_value)) // modify register entry
		return false;

	return true;
}

bool ECFOptimizerState::readVectorString(std::string vecStr, double* vector)
{
	// temporary regex string
	std::string str_regex = vecStr;

	std::smatch sm; // matches (regex)
	std::regex regex(R"(\((-?[\.\s\d]+),(-?[\.\s\d]+),(-?[\.\s\d]+)\))"); // regex match (11,222,3333), (-2,-8,-23), (54.32,- 5.42 , -43.22), ...

	// search output string for coverage value (regex)
	if (std::regex_search(str_regex, sm, regex))
	{
		std::string x = sm.str(1);
		std::string y = sm.str(2);
		std::string z = sm.str(3);

		vector[0] = stod(sm.str(1));
		vector[1] = stod(sm.str(2));
		vector[2] = stod(sm.str(3));
	}

	return true;
}