#include <ecf/ECF.h>
#include "OptimizerUnit/FunctionMinEvalOp.h"
#include <time.h>

//#include <QProcess> // Qt (run batch)
//#include <QDebug>	// Qt (run batch)
//
//#define WIN32_LEAN_AND_MEAN // createProcess (run batch)
#include <windows.h>

#include <regex> // regex


FunctionMinEvalOp::FunctionMinEvalOp(DataFarming::ExecutionService* service, DataFarming::DataFarmingExperimentFile* dfe, ParameterVariationStruct& pv)
{
	// read data farming model
	DataFarming::ExecutionService::Models models;
	service->getModels(models);
	assert(!models.empty() && "No models exists!");

	// initialize variable for data farming
	_modelInfo = models[0];
	_executionService = service;
	_dfe = dfe;
	_seed = dfe->getDataFarmingExperiment()->getExperimentalSeed();
	_parameterVariationStruct = pv;
}

void FunctionMinEvalOp::registerParameters(StateP state)
{
	// register new parameters from parameters.txt
	state->getRegistry()->registerEntry("function", (voidP)(new uint(1)), ECF::UINT);
	state->getRegistry()->registerEntry("fit.colname", (voidP)(new std::string), ECF::STRING);
	state->getRegistry()->registerEntry("fit.function", (voidP)(new std::string), ECF::STRING);
	state->getRegistry()->registerEntry("coverage.resultpath", (voidP)(new std::string), ECF::STRING);
}


bool FunctionMinEvalOp::initialize(StateP state)
{
	/* initialize random seed
	 * TODO: use seed from df
	 */
	//srand(time(NULL));

	// state
	_state = state;

	// PARMETER: function choice
	voidP sptr = state->getRegistry()->getEntry("function"); // get parameter value
	iFunction_ = *((uint*)sptr.get()); // convert from voidP to user defined type

	// PARMETER: name of fitness column
	sptr = state->getRegistry()->getEntry("fit.colname"); // get parameter value
	fitnessColName_ = *((std::string*)sptr.get()); // convert from voidP to user defined type

	// PARMETER: fitness function
	sptr = state->getRegistry()->getEntry("fit.function"); // get parameter value
	fitnessFunction_ = &*((std::string*)sptr.get()); // convert from voidP to user defined type

	// PARMETER: population size
	sptr = state->getRegistry()->getEntry("population.size"); // get parameter value
	populationSize_ = *((uint*)sptr.get()); // convert from voidP to user defined type

	// PARMETER: population size
	sptr = state->getRegistry()->getEntry("coverage.resultpath"); // get parameter value
	coverageResultPath_ = *((std::string*)sptr.get()); // convert from voidP to user defined type

	// dimension of coordinates
	_dimension = 1; // default; update follows in evaluate(); same value for all genotypes

	return true;
}

/**
 * start DF experiment
 */
bool FunctionMinEvalOp::startDfExperiment(IndividualP individual)
{
	//// number of genotype variables
	//// e.g. 10 sensor positions means 10 <FloatingPoint> variables
	//_numberOfGenotypes = individual->size();

	//std::string name = individual->getGenotype()->getName();

	////
	//// parse genotype and add to vector
	////

	////  - FloatingPoint vector
	//std::vector<FloatingPoint::FloatingPoint*> floatingPoints;
	//floatingPoints.reserve(_numberOfGenotypes);

	////  - BitString vector
	//std::vector<BitString::BitString*> bitStrings;
	//bitStrings.reserve(_numberOfGenotypes);

	//// add to vector
	//for (int i = 0; i < _numberOfGenotypes; i++)
	//{
	//	if (individual->getGenotype(i)->getName() == "FloatingPoint")	{ floatingPoints.push_back((FloatingPoint::FloatingPoint*)individual->getGenotype(i).get()); }
	//	if (individual->getGenotype(i)->getName() == "BitString")		{ bitStrings.push_back((BitString::BitString*)individual->getGenotype(i).get()); }
	//}

	//// dimension of coordinates
	//// use first FloatingPoint genotype and get number of values
	//// PRECONDITION: all FloatingPoint variables have same dimension
	//_dimension = floatingPoints[0]->realValue.size();

	//// FloatingPoint
	//for (FloatingPoint::FloatingPoint* floatGen : floatingPoints)
	//{
	//	std::cout << std::endl;
	//	for (int i = 0; i < floatGen->realValue.size(); i++)
	//	{
	//		if (floatGen->realValue.size() == 1)
	//		{
	//			std::cout << "ECF created value = " << floatGen->realValue[i] << " // dimension = " << floatGen->realValue.size();
	//		}
	//		else
	//		{
	//			std::cout << "ECF created value with index " << i << " = " << floatGen->realValue[i] << " // dimension = " << floatGen->realValue.size();
	//		}
	//	}
	//}

	//// BitString
	//for (BitString::BitString* bitGen : bitStrings)
	//{
	//	// output: BitString value
	//	std::cout << "BitString: ";
	//	for (int i = 0; i < bitGen->bits.size(); i++)
	//	{
	//		std::cout << bitGen->bits.at(i);
	//	}
	//	std::cout << std::endl;
	//}

	//std::cout << std::endl;

	////////////////////////////////////////////////////
	//// DATA FARMING //////////////////////////////////
	////////////////////////////////////////////////////
	//// data farming process

	//// 1. modify dfe file for next experiment
	//setOptimizerDesign(floatingPoints);

	//// 2. start df experiment
	//executeExperiment(*_executionService);

	//// 3. get study information
	//const DataFarming::StudyInfo& studyInfo = *_executionService->getInfo(_currentStudyId);

	//// 4. wait till experiment is finished (not a good solution)
	//while (!studyInfo.getStatus().isFinished())
	//{
	//	_sleep(1); // msec
	//	QCoreApplication::processEvents();
	//}

	//// 5. save study id and generation number
	//_currentGenerationStudyIds.push_back(_currentStudyId);

	////_studyIdAndGenerationNo[_currentStudyId] = _state->getContext()->generationNo_;
	//
	//// end of data farming process
	////////////////////////////////////////////////////

	//// finished DF experiments

	return true;
}

uint idIndexCounter = 0;
uint evaluationCountTestFun = 0;
FitnessP FunctionMinEvalOp::evaluate(IndividualP individual)
{
	// fitness value
	double fitnessValue = 0;

	// evaluation creates a new fitness object using a smart pointer
	// in our case, we try to minimize the function value, so we use FitnessMin fitness (for minimization problems)
	FitnessP fitness(new FitnessMin);

	// number of genotype variables
	_numberOfGenotypes = individual->size();

	std::string name = individual->getGenotype()->getName();

	/////////////////////////////////////////////////////////////////////////////////
	//// parse genotype and update parameter variations struct
	updateParameterVariations(individual);

	// select function
	// can be defined in parameters.txt (<Registry><Entry key=function>)
	switch (iFunction_) {

		// Use DF to compute fitness value  (input: 4 FloatingPoints)
	case 1:
	{
		//////////////////////////////////////////////////
		// data farming process

		// 1. update dfe file for next experiment
		setOptimizerDesign();

		// 2. start df experiment
		executeExperiment(*_executionService);

		// 3. get study information
		const DataFarming::StudyInfo& studyInfo = *_executionService->getInfo(_currentStudyId);

		// timeout settings
		time_t timer_begin = time(NULL);
		_timeoutReached = false;
		
		// 4. wait till experiment is finished (not a good solution)
		while (!studyInfo.getStatus().isFinished())
		{
			int elapsedTime = time(NULL) - timer_begin; // compute elapsed time since start of while-loop

			// if timeout is set
			if (_dfe->getDataFarmingExperiment()->isSingleRunTimeOutSet())
			{
				if (elapsedTime >= _dfe->getDataFarmingExperiment()->getSingleRunTimeOutSec())
				{
					_timeoutReached = true;
					break;
				}
			}

			_sleep(1); // msec
			QCoreApplication::processEvents();
		}

		// 5. compute fitness value
		if (!_timeoutReached)
		{
			fitnessValue = computeFitness(idIndexCounter++); // normal case
		}
		else
		{
			fitnessValue = 1000000000.0; // special case
		}

		// end of data farming process
		//////////////////////////////////////////////////
	} break;

	// DF test function: similar input, no data farming (input: 4 FloatingPoints)
	// parameter: 4 FloatingPoints; dimension=1
	// fitness = distance between 2 points
	case 2: {
		//// point 1
		//double x1 = floatingPoints[0]->realValue[0]; // realValue[0], because dimension=1
		//double y1 = floatingPoints[1]->realValue[0];

		//// point 2
		//double x2 = floatingPoints[2]->realValue[0];
		//double y2 = floatingPoints[3]->realValue[0];

		//// distance between point 1 and point 2
		//double distance = sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2));

		//fitnessValue = distance;
	} break;

	// Coverage test function
	case 9:
	{
		//fitnessValue = std::rand() % 10000;
		fitnessValue = 999;
		std::cout << "+--------------------------------------------------" << std::endl;
		std::cout << "| evaluation: " << evaluationCountTestFun++ << ", const fitness: " << fitnessValue << std::endl;
		std::cout << "+--------------------------------------------------" << std::endl;

	} break;

	default:
		throw("FunctionMinEvalOp: invalid function index in configuration!");
	}

	std::cout << std::endl;

	// save fitness value
	//_meanFitnessValues.push_back( fitnessValue );
	//_studyIdFitness[_currentStudyId] = fitnessValue;

	// file stream header
	if (evaluationsHeaderIsEmpty) {
		// write header
		if (!writeEvaluationHeader()) {
			return false;
		}
		evaluationsHeaderIsEmpty = false; // now evaluations header is set
	}

	// write current evaluation to file
	writeEvaluation(_fileStream, _currentStudyId, fitnessValue);

	// set and return fitness value
	fitness->setValue(fitnessValue);
	return fitness;
}

void FunctionMinEvalOp::runNextExperiment(DataFarming::ExecutionService& service, const DataFarming::StudyInfo& studyInfo)
{
	// this function is called every update frame inside the execution service
	if (!studyInfo.getStatus().isFinished())
	{
		// not finished currently, ignore this update frame
		// for more information of the possible status, see DataFarming::StudyStatus
		return;
	}
	std::cout << "| ...DF experiment DONE for study ID " << _currentStudyId << std::endl;
	std::cout << "+---------------------------------" << std::endl;

	bool _experimentFinished = true;
	// if experiment is finished
	// get fitness
	// write fitness
	// add fitness to state/algorithm/...
	// start next experiment

}

void FunctionMinEvalOp::executeExperiment(DataFarming::ExecutionService& service)
{
	std::cout << "+---------------------------------" << std::endl;
	std::cout << "| Starting data farming experiment..." << std::endl;
	//const DataFarming::StudyInfo* studyInfo = service.getInfo(_currentStudyId);

	DataFarming::SubmitContext context;

	context._dfe = _dfe;
	context._priority = 0;
	context._updateCallback = DataFarming::SubmitContext::UpdateCallback::fromMethod< FunctionMinEvalOp, &FunctionMinEvalOp::runNextExperiment >(this);
	context._modelInfo = _modelInfo;

	// save StudyID and start experiment
	_currentStudyId = service.submit(context); // submit start experiment

	//studyInfo = service.getInfo(_currentStudyId);
}

/**
 * modify DFE for next experiment
 */
void FunctionMinEvalOp::setOptimizerDesign()
{
	// get DoE from DFE object
	DataFarming::ExperimentalDesign& doe = _dfe->getDataFarmingExperiment()->getExperimentalDesign();

	// get all sub designs from DoE
	DataFarming::ExperimentalDesign::Subdesigns& subdesigns = doe.getSubdesigns();
	for (Core::ref_ptr<DataFarming::Subdesign>& subdesign : subdesigns)
	{
		// only select OPTIMIZER designs
		if (!subdesign->isDesignType(DataFarming::OPTIMIZER))
		{
			continue;
		}

		// index for numbers and enumerations
		int iNumber = 0;
		int iEnumeration = 0;

		// iterate over all parameter variations
		DataFarming::Subdesign::ParameterVariations& pvs = subdesign->getParameterVariations();
		for (Core::ref_ptr<DataFarming::ParameterVariation>& pv : pvs)
		{
			assert(pv->getNumOfValues() == 1 && "Invalid number of variation values!");

			// all parameter inside a parameter variation have the same data type and ranges (if set)
			assert(pv->getNumOfParameters() > 0 && "DoE is invalid!");

			DataFarming::Parameter* p = pv->getParameter(0);

			if (p->isType(DataFarming::ENUMERATION))
			{
				// variation name
				std::string varName = pv->getVariationName();

				// categorical variable: must be last parameter variation in DFE file (bad)
				if (!p->getEnumValues().empty())
				{
					std::string name = pv->getVariationName();
					std::cout << name << "(dfe) = " << "\"" << pv->getStringValues()[0] << "\"";

					// update DFE parameter
					pv->getStringValues()[0] = _parameterVariationStruct.enumerations[iEnumeration].stringValue;

					std::cout << " is replaced with value of ECF / new value = " << "\"" << pv->getStringValues()[0] << "\"" << std::endl;
				}
				else
				{
					// nothing to change
				}
				iEnumeration++;
			}
			else
			{
				// numeric variable
				Core::Patterns::IntOptional decimalPlaces = p->getDecimalPlaces();

				assert(pv->getDoubleValues().size() == 1 && "DoE is invalid");

				// variable name
				std::string name = pv->getVariationName();
				std::cout << name << "(dfe) = " << pv->getDoubleValues()[0];

				// set variable of dfe for next experiment
				if (_parameterVariationStruct.numbers.size() > iNumber)
				{
					pv->getDoubleValues()[0] = _parameterVariationStruct.numbers[iNumber].doubleValue;
				}
				else
				{
					std::cout << std::endl << "-----" << std::endl;
					assert(_parameterVariationStruct.numbers.size() > iNumber && "Number of genotype values in framework wrong. Must be the same as in DFE file.");
				}

				std::cout << " is replaced with value of ECF = " << pv->getDoubleValues()[0] << std::endl;

				iNumber++;
			}
		}
	}
}

/**
 * parse output file and compute fitness value
 */
double FunctionMinEvalOp::computeFitness(uint i)
{
	double fitnessValue;
	Core::String filename, filepath;
	Core::String studyIdString;

	// convert studyId to Core::String
	if (_currentGenerationStudyIds.empty())
	{
		studyIdString = std::to_string(_currentStudyId);
	}
	else
	{
		studyIdString = std::to_string(_currentGenerationStudyIds.front());
		_currentGenerationStudyIds.erase(_currentGenerationStudyIds.begin());
	}



	if (_executionService->getName() == "CoverageApplication")
	{
		filepath = coverageResultPath_;
	}
	else
	{
		filename = R"(\output.csv)";
		filepath = R"(data\DataFarming\)" + _executionService->getName() + R"(\)" + studyIdString + filename;
	}

	// path of result file

	// save results of studyID in 'filepath'
	//_executionService->getResultFile(_currentStudyId, filepath);

	// model to access csv data
	QAbstractItemModel* csvModel = CSVItemModel::create(QString::fromLatin1(filepath.c_str()));

	if (csvModel == nullptr)
	{
		std::cout << "\nCould not read csv file!" << std::endl;
	}

	// get row and column count
	int rows = csvModel->rowCount(); // known ERRORS: wrong DFE || wrong EXE || wrong argument in TestFunctionsApplicaion.xml (execution service)
	int cols = csvModel->columnCount();

	// check if a fitness function is defined in parameters.txt
	// get values of csv
	// modify function string, so it can be computed
	if (*fitnessFunction_ != "0")
	{
		// in that dynamic fitness function all column name are replaced with values
		// original fitness function MUST NOT be manipulated (needed in later evaluations)
		std::string dyn_fitnessFunction = *fitnessFunction_;

		// temporary regex string
		std::string str_regex = *fitnessFunction_;

		// search fitness function for column names
		// column names can be identified with: "columnName"
		std::smatch sm; // matches (regex)
		std::regex regex("(.*?)(\"(.+?)\")"); // regex

		// search output string for coverage value (regex)
		while (std::regex_search(str_regex, sm, regex))
		{
			std::string sign = sm.str(1); // sign / prefix: */+-
			std::string colName = sm.str(3); // column name

			std::regex rx(sm.str(2)); // column name with (") and sign: this string will be replaced (e.g.: -"coverage")

			// get column value
			// looking for column for computing fitness
			int columnNumber = 0;
			for (; columnNumber < cols; ++columnNumber) {
				if (csvModel->headerData(columnNumber, Qt::Horizontal, Qt::DisplayRole).toString() == new QString(colName.c_str())) {

					// read value from csv
					//QModelIndex columnValueIndex = csvModel->index(0, columnNumber);
					//QString columnValue = csvModel->data(columnValueIndex, Qt::DisplayRole).toString();

					double columnAverage = getColumnAverage(csvModel, columnNumber);
					
					// replace placeholders with values from csv
					dyn_fitnessFunction = std::regex_replace(dyn_fitnessFunction, rx, std::to_string(columnAverage)); // replace string

					_resultsColumnValues[colName] = columnAverage;
					
					break; // leave for-loop
				}
				// if column name was found than break should has been executed
				// if this was not the case, colName do not exist in result file
				if (columnNumber == cols - 1) {
					dyn_fitnessFunction = std::regex_replace(dyn_fitnessFunction, rx, "0"); // replace string
				}
				//assert(columnNumber != cols - 1 && "Could not find fitness column. Check parameters.txt or output file");
			}

			str_regex = sm.suffix(); // shorten regex string
		}

		// compute string with arithmetic expression
		fitnessValue = computeExpression(dyn_fitnessFunction);
	}
	else
	{
		// default (fit.function==0)
		//  get column value
		//  looking for column for computing fitness
		int columnNumber = 0;
		for (; columnNumber < cols; ++columnNumber) {
			if (csvModel->headerData(columnNumber, Qt::Horizontal, Qt::DisplayRole).toString() == new QString(fitnessColName_.c_str())) {
				break;
			}
			assert(columnNumber != cols - 1 && "Could not find fitness column. Check parameters.txt or output file");
		}

		// read value from csv
		QModelIndex columnValueIndex = csvModel->index(0, columnNumber);
		double columnValue = csvModel->data(columnValueIndex, Qt::DisplayRole).toFloat();

		fitnessValue = columnValue;
	}

	std::cout << "| Fitness(id=" << studyIdString << ") = " << fitnessValue << std::endl;
	std::cout << "+----------------------------------------------------------------------" << std::endl;

	return fitnessValue;
}

/**
 * compute a string with an arithmetic expression
 * supported operators: ADDITION (+), SUBTRACTION (-)
 * NOT supported: parantheses, sinus, ...
 */
double FunctionMinEvalOp::computeExpression(std::string expression)
{
	double result = -1;

	std::string str_regex = expression;

	std::smatch sm; // matches (regex)
	//std::regex regex("([+-]?)([\\d\.]+)"); // regex
	std::regex regex("([*/+-]?)((\\d|\\.)+|\"(.+?)\")"); // regex

	std::vector<std::string> operators; // operator/sign ( */+- )
	std::vector<double> operands;

	// search output string for coverage value (regex)
	while (std::regex_search(str_regex, sm, regex))
	{
		// expression can be parsed; set result = 0
		result = 0;

		// operator/sign ( */+- )
		if (sm.str(1) == "")
		{
			operators.push_back("+");
		}
		else
		{
			operators.push_back(sm.str(1));
		}

		// value
		std::string valueStr = sm.str(2);

		// convert operand to double
		double operand = strtod(valueStr.c_str(), NULL);

		// push operand to vector
		operands.push_back(operand);

		// short regex string
		str_regex = sm.suffix();
	}

	//std::cout << std::endl;

	// compute arithmetic expression
	for (int i = 0; i < operators.size(); i++)
	{
		if (operators.at(i) == "+")
		{
			result += operands.at(i);
			continue;
		}
		if (operators.at(i) == "-")
		{
			result -= operands.at(i);
			continue;
		}
		if (operators.at(i) == "*")
		{
			result *= operands.at(i);
			continue;
		}
		if (operators.at(i) == "/")
		{
			result /= operands.at(i);
			continue;
		}
	}

	//std::cout << " = result = " << result << std::endl;

	return result;
}

/**
 * write file stream header of evaluations.csv
 */
bool FunctionMinEvalOp::writeEvaluationHeader()
{
	_filepath = R"(data\DataFarming\)" + _executionService->getName() + R"(\results\)"; // file path

	_filename = R"(evaluations.csv)"; // file name

	// create new file, overwrite old (if existing)
	_fileStream.open(_filepath + _filename);

	if (!_fileStream.is_open())
	{
		std::wstring stemp = std::wstring(_filepath.begin(), _filepath.end());
		LPCWSTR sw = stemp.c_str();
		CreateDirectory(sw, NULL);

		std::cout << "********************\n****\n**** WARNING:" << std::endl;
		std::cout << "**** Could not open /results/evaluations.csv\n**** Creating folder " << _filepath << " !!!" << std::endl;
		std::cout << "****\n********************" << std::endl;
		return false;
	}

	// header string
	std::string header = "generation;study id;fitness;";

	for (uint n = 0; n < _parameterVariationStruct.numbers.size(); n++) {
		header += "number;";
	}

	for (uint e = 0; e < _parameterVariationStruct.enumerations.size(); e++) {
		header += "enumeration;";
	}

	// write all column names that are defined in fitness function of parameters.txt
	for (auto const& col : _resultsColumnValues) {
		header += col.first;
		header += ";";
	}

	_fileStream << header << "\n";
		
	_fileStream.close();

	return true;
}

/**
 * write study id and fitness value immediately after fitness computation
 */
bool FunctionMinEvalOp::writeEvaluation(ofstream& file_stream, uint study_id, double fitness_value)
{
	// create new file, overwrite old (if existing)
	file_stream.open(_filepath + _filename, ios::app);

	// check if file is open
	if (file_stream.is_open())
	{
		file_stream << _state->getGenerationNo() << ";" << study_id << ";" << fitness_value; // generation; studyId; fitness

		// floating points / numbers
		for (NumberStruct number : _parameterVariationStruct.numbers)
		{
			file_stream << ";" << number.doubleValue;
		}

		// binaries / enumerations
		for (EnumerationStruct enumeration : _parameterVariationStruct.enumerations)
		{
			file_stream << ";" << enumeration.stringValue;
		}

		// write all result column values that are defined in fitness function of parameters.txt
		for (auto const& col : _resultsColumnValues) {
			file_stream << ";" << col.second;
		}

		file_stream << "\n";
		file_stream.close();
	}
	else { return FALSE; }

	return TRUE;
}

/**
* update parameter variations from dfe
* this will update _parameterVariationStruct with new values generated from framework
*/
void FunctionMinEvalOp::updateParameterVariations(IndividualP individual)
{
	std::cout << "-----------------------------------------------------------------------" << std::endl;

	// FloatingPoints
	std::vector<FloatingPoint::FloatingPoint*> floatingPoints;
	floatingPoints.reserve(_numberOfGenotypes);

	// BitStrings
	std::vector<BitString::BitString*> bitStrings;
	bitStrings.reserve(_numberOfGenotypes);

	// Binaries
	std::vector<Binary::Binary*> binaries;
	binaries.reserve(_numberOfGenotypes);

	// get genotype of individual
	for (int i = 0; i < _numberOfGenotypes; i++)
	{
		if (individual->getGenotype(i)->getName() == "FloatingPoint")	{ floatingPoints.push_back((FloatingPoint::FloatingPoint*)individual->getGenotype(i).get()); }
		if (individual->getGenotype(i)->getName() == "BitString")		{ bitStrings.push_back((BitString::BitString*)individual->getGenotype(i).get()); }
		if (individual->getGenotype(i)->getName() == "Binary")			{ binaries.push_back((Binary::Binary*)individual->getGenotype(i).get()); }
	}

	// add floating points to ParameterVariationStruct
	addFloatingPoints(floatingPoints);

	// add bit strings to ParameterVariationStruct (not used)
	addBitStrings(bitStrings);

	// add binaries to ParameterVariationStruct (not used)
	addBinaries(binaries);

	std::cout << "---------------------------------" << std::endl;
}

/**
 * transform vector3D string to double vector/array
 */
double* FunctionMinEvalOp::vecStrToDouble(std::string vecStr3D)
{
	double vec[3] = {0,0,3};

	// temporary regex string
	std::string str_regex = vecStr3D;

	std::smatch sm; // matches (regex)
	std::regex regex(R"(\((-?[\.\s\d]+),(-?[\.\s\d]+),(-?[\.\s\d]+)\))"); // regex match (11,222,3333), (-2,-8,-23), (54.32, 5.42 , 43.22), ...

	// search output string for coverage value (regex)
	if (std::regex_search(str_regex, sm, regex))
	{
		vec[0] = stod(sm.str(1)); // x
		vec[1] = stod(sm.str(2)); // y
		vec[2] = stod(sm.str(3)); // z
	}
	
	return vec;
}

/**
 * compute column average
 * parameter: CSV model, fitness column number
 */
double FunctionMinEvalOp::getColumnAverage(QAbstractItemModel* csvModel, int fitnessColumnNumber)
{
	double sum = 0;
	int invalidRows = 0; // for RIKOV

	// get "status" column number (only RIKOV)
	int statusColumnNumber = 0;
	for (; statusColumnNumber < csvModel->columnCount(); ++statusColumnNumber) {
		if (csvModel->headerData(statusColumnNumber, Qt::Horizontal, Qt::DisplayRole).toString() == "status") {
			break;
		}
	}

	for (int row = 0; row < csvModel->rowCount(); row++)
	{

		// read status column value from csv
		QModelIndex statusColumnValueIndex = csvModel->index(row, statusColumnNumber);
		std::string statusColumnValue = csvModel->data(statusColumnValueIndex, Qt::DisplayRole).toString().toStdString();

		// read fitness column value
		QModelIndex columnValueIndex = csvModel->index(row, fitnessColumnNumber);
		double colValue = csvModel->data(columnValueIndex, Qt::DisplayRole).toDouble();

		// (only RIKOV) if status column value == "timestep", increment invalidRows
		if (statusColumnValue == "timestep")
		{
			invalidRows++;
		}
		else
		{
			sum += colValue;
		}
	}

	// do not use "timestep" rows for average computation
	int validRows = csvModel->rowCount() - invalidRows;

	double average = sum / validRows;

	return average;
}

/**
 * add floating point values, generated by ECF, to ParameterVariationStruct
 */
void FunctionMinEvalOp::addFloatingPoints(std::vector<FloatingPoint::FloatingPoint*>& floatingPoints)
{
	// index for floating points
	uint indexFloats = 0;

	// dimension of coordinates
	// use first FloatingPoint genotype and get number of values
	// PRECONDITION: all FloatingPoint variables have same dimension
	_dimension = floatingPoints[0]->realValue.size();

	//  - FloatingPoint / numbers
	for (uint iNum = 0; iNum < _parameterVariationStruct.numbers.size(); iNum++)
	{
		_parameterVariationStruct.numbers[iNum].doubleValue = floatingPoints[indexFloats]->realValue[0]; // update _parameterVariationStruct

		// output: FloatingPoint value
		std::cout << "FloatingPoint generated by ECF: ";
		std::cout << floatingPoints[indexFloats]->realValue[0] << std::endl;

		indexFloats++;
	}

	//  - FloatingPoint / enumerations
	if (_parameterVariationStruct.numbers.size() != floatingPoints.size()) // TRUE if FlotingPoint variables are used for enumerations
	{
		uint indexEnum = 0; // index for enumerations
		for (; indexEnum < _parameterVariationStruct.enumerations.size(); indexEnum++) // TODO: vereinfachen!
		{
			// default case
			if (_parameterVariationStruct.enumerations[indexEnum].type == "enumeration")
			{
				// rounding (floor)
				double val = floatingPoints[indexFloats]->realValue[0];
				int r_floor = floor(val);

				_parameterVariationStruct.enumerations[indexEnum].doubleValue = val; // update _parameterVariationStruct
				_parameterVariationStruct.enumerations[indexEnum].stringValue = _parameterVariationStruct.enumerations[indexEnum].enumValues[r_floor]; // update _parameterVariationStruct

				// output: FloatingPoint value
				std::cout << "FloatingPoint generated by ECF: ";
				std::cout << floatingPoints[indexFloats]->realValue[0] << " // dimension = " << floatingPoints[indexFloats]->realValue.size() << std::endl;

				indexFloats++;
			}

			// speacial case for RiKoV
			if (_parameterVariationStruct.enumerations[indexEnum].type == "vector")
			{
				// floating point value
				double xVal = floatingPoints[indexFloats]->realValue[0];
				double yVal = floatingPoints[indexFloats + 1]->realValue[0];

				// use z value of DFE (z value is NOT varied)
				double zVal = vecStrToDouble(_parameterVariationStruct.enumerations[indexEnum].stringValue)[2];

				_parameterVariationStruct.enumerations[indexEnum].doubleValue = xVal; // update _parameterVariationStruct
				_parameterVariationStruct.enumerations[indexEnum].stringValue = "(" + std::to_string(xVal) + "," + std::to_string(yVal) + "," + std::to_string(zVal) + ")"; // update _parameterVariationStruct

				// output: FloatingPoint value
				std::cout << "FloatingPoint generated by ECF: ";
				std::cout << floatingPoints[indexFloats]->realValue[0] << " // special case for RiKoV (vector)" << std::endl;
				std::cout << "FloatingPoint generated by ECF: ";
				std::cout << floatingPoints[indexFloats + 1]->realValue[0] << " // special case for RiKoV (vector)" << std::endl;

				// increment two times: two floating points used
				indexFloats++;
				indexFloats++;
			}
		}
	}

}

/**
* add bit string values, generated by ECF, to ParameterVariationStruct
*/
void FunctionMinEvalOp::addBitStrings(std::vector<BitString::BitString*>& bitStrings)
{
	// - BitString (not used)
	for (BitString::BitString* bitGen : bitStrings)
	{
		// output: BitString value
		std::cout << "BitString: ";
		for (int i = 0; i < bitGen->bits.size(); i++)
		{
			// update _parameterVariationStruct
			for (uint k = 0; k < _parameterVariationStruct.enumerations.size(); k++)
			{
				if (_parameterVariationStruct.enumerations[k].enumValues.size() == 2)
				{
					_parameterVariationStruct.enumerations[k].stringValue = _parameterVariationStruct.enumerations[k].enumValues[bitGen->bits.at(i)];
				}
			}
			std::cout << bitGen->bits.at(i);
		}
		std::cout << std::endl;
	}
}

/**
* add binary values, generated by ECF, to ParameterVariationStruct
*/
void FunctionMinEvalOp::addBinaries(std::vector<Binary::Binary*>& binaries)
{
	// - Binary (not used)
	for (int k = 0; k < binaries.size(); k++) //for (Binary::Binary* binaryGen : binaries)
	{
		// output: BitString value
		std::cout << "Binary generated by ECF: ";
		for (uint i = 0; i < binaries[k]->realValue.size(); i++)
		{

			// _parameterVariationStruct represents dfe
			// _parameterVariationStruct has parameter of type NUMBER and ENUMERATION
			// set stringValue of enumeration parameter to a new value
			// choose from enumValues; use index given from framework
			// example: framework gives a 1 --> enumValues with index 1 is "green" --> "green" is new stringValue of enumeration parameter

			EnumerationStruct* e = &_parameterVariationStruct.enumerations[k];
			Binary::Binary* b = binaries[k];

			e->stringValue = e->enumValues[(b->realValue[i])]; // update _parameterVariationStruct

			//std::cout << "1. " << binaries[k]->realValue[i] << "(" << _parameterVariationStruct.enumerations[k].stringValue << ") ";
			std::cout << b->realValue[i] << "(" << e->stringValue << ") ";

			//std::cout << binaries[k]->realValue[i];
		}
		std::cout << std::endl;
	}
}

/**
* transform spherical coordinates to cartesian/geocentric coordinates (x, y, z)
*
* theta:	inclination in degree (dt. Polarwinkel)
* phi:		azimuth in degree
* r:		radius (default: radius of earth)
*/
//void FunctionMinEvalOp::transformToGeocentricCoordinates(double* geocentricCoordinates, double theta, double phi, double r)
//{
//	std::cout << "Transform spherical coordinates to cartesian/geocentric coordinates (x, y, z)..." << std::endl;
//
//	const double PI = 3.141592653589793; // also possible: double pi = 4 * atan(1);
//
//	// transform degree to radian
//	double theta_rad = theta / 180 * PI;
//	double phi_rad = phi / 180 * PI;
//
//	double x = r * sin(theta_rad) * cos(phi_rad);
//	double y = r * sin(theta_rad) * sin(phi_rad);
//	double z = r * cos(theta_rad);
//
//	geocentricCoordinates[0] = x;
//	geocentricCoordinates[1] = y;
//	geocentricCoordinates[2] = z;
//
//	std::cout << "Cartesian/geocentric coordinates (x, y, z):\n(" << x << ", " << y << ", " << z << ")" << std::endl;
//}

///** MOVED TO COVERAGEAPPLICATION
// * coverage optimization using GRASS GIS
// * 
// */
//float FunctionMinEvalOp::computeCoverage(double x, double y, double z)
//{
//	float coverage = 0.f; // non-visible coverage value
//
//	bool testcase = false;
//
//	// use methode
//	//
//	// "cp" (createProcess)	: does work :) // can only read exit code :(
//	// "qt" (Qt)			: does work // can read print output from python script
//	// "py" (python)		: does work // can read print output from python script
//	std::string method = "cp"; // possible methods: "qt", "cp", "py"
//
//	// path of python.exe
//	QString pythonPath = R"(C:\Program Files (x86)\Python27\python.exe)";
//
//	// path of python script
//	QString pythonScript_test = R"(C:\temp\batch\PythonCode\StartGisShell.py)";
//	QString pythonScript = R"(D:\Projekte\GrassGisInterface\ComandViewshedAnalysis\Python Code\StartGisShell.py)";
//
//	// path of geo file
//	QString geofile = R"(C:\temp\batch\Geofiles\Data\52.dt2)";
//
//	// batch file: startgis.bat
//	QString startgis = R"(D:\Projekte\GrassGisInterface\ComandViewshedAnalysis\startgis)";
//
//	// TIF file
//	QString tif = R"(D:\Projekte\GrassGisInterface\ComandViewshedAnalysis\Geofiles\Optimierer_Input_2016_08_30\resultDataFile.tif)";
//
//	// AOO
//	QString aoo = R"(D:\Projekte\GrassGisInterface\ComandViewshedAnalysis\Geofiles\Optimierer_Input_2016_08_30\AOO\AOO.shp)";
//
//	// python input file
//	QString pythonInput = R"(D:\Projekte\GrassGisInterface\ComandViewshedAnalysis\Geofiles\SensorList.xml)";
//
//	// coordinates
//	double xCoordinate = x; // test value: 11.53153896; new: 1316398.500;
//	double yCoordinate = y; // test value: 52.02578735; new: 6917277.620;
//	QString qx = QString::number(xCoordinate, 'f', 3);
//	QString qy = QString::number(yCoordinate, 'f', 3);
//
//	// command line for create process
//	std::string commandLine_createProcess;
//	std::string cpx = to_string(xCoordinate);
//	if ( testcase) commandLine_createProcess = R"(C:\temp\batch\startgis )" + geofile.toStdString() + " " + to_string(11.53153896) + " " + to_string(52.02578735);
//	if (!testcase) commandLine_createProcess = startgis.toStdString() + " " + tif.toStdString() + " " + aoo.toStdString() + " " + pythonInput.toStdString();
//
//	//
//	// run bat (createProcess)
//	// output of python and GIS on console
//	//
//	if (method == "cp")
//	{
//		coverage = batch_createProcess(commandLine_createProcess);
//	}
//
//	//
//	// run bat (Qt)
//	// Qt can read print commands of python script
//	// no GIS output but python print output
//	// does not work with a MAXDISTANCE ove 21000 (see config.ini)
//	//
//	if (method == "qt")
//	{
//		QProcess p;
//		if ( testcase) p.start("cmd.exe", QStringList() << "/c" << R"(C:\temp\batch\startgis)" << geofile << QString::number(11.53153896) << QString::number(52.02578735));
//		if (!testcase) p.start("cmd.exe", QStringList() << "/c" << startgis << tif << aoo << pythonInput);
//		p.waitForFinished();
//
//
//
//
//
//
//
//		// output string (all print outputs of python)
//		std::string output = p.readAllStandardOutput().toStdString();
//
//		// matches (regex)
//		std::smatch sm;
//
//		// search output string for coverage value (regex): python output: '#coverage; result;'
//		if (std::regex_search(output, sm, std::regex(".*#coverage:(.*?);.*")))
//		{
//			// string found (convert to float)
//			if (!(coverage = std::stof(sm[1])))
//				std::cout << "ERROR: coverage string is 0 or could not be converted to float" << std::endl;
//		}
//
//		if (coverage == 0)
//		{
//			//coverage = 1.0f * p.exitCode() / 100;
//			std::cout << "exit code: " << p.exitCode() << std::endl;
//		}
//		std::cout << "Coverage: " << coverage << std::endl;
//		
//
//
//
//
//		std::string str = p.readAllStandardOutput().toStdString();
//		std::cout << "Standard output: " << str << std::endl;
//		std::cout << "exit code: " << p.exitCode() << std::endl;
//		qDebug() << p.readAllStandardOutput();
//	}
//
//	//
//	// run python script (Qt)
//	// Qt can read print commands of python script
//	// no GIS and no python output on console
//	//
//	if (method == "py")
//	{
//		// create and start Qt process
//		QProcess p;
//		if ( testcase) p.start(pythonPath, QStringList() << pythonScript_test << geofile << QString::number(11.53153896) << QString::number(52.02578735));
//		if (!testcase) p.start(pythonPath, QStringList() << pythonScript << tif << aoo << qx << qy);
//		p.waitForFinished();
//
//		// output string (all print outputs of python)
//		std::string output = p.readAllStandardOutput().toStdString();
//
//		// matches (regex)
//		std::smatch sm;
//		
//		// search output string for coverage value (regex): python output: '#coverage; result;'
//		if (std::regex_search(output, sm, std::regex(".*#coverage:(.*?);.*")))
//		{
//			// string found (convert to float)
//			if (!(coverage = std::stof(sm[1])))
//				std::cout << "ERROR: coverage string could not be converted to float." << std::endl;
//		}
//
//		if (coverage == 0)
//		{
//			//coverage = 1.0f * p.exitCode() / 100;
//			std::cout << "exit code: " << p.exitCode() << std::endl;
//		}
//
//		qDebug() << p.readAllStandardOutput();
//	}
//
//
//	//std::cout << "--end--" << std::endl;
//
//	return coverage;
//}
//
///**
// * run batch file
// *
// * batch file starts python script and compute coverage
// * coverage is stored in exit code of python script
// *
// */
//float FunctionMinEvalOp::batch_createProcess(std::string commandLine)
//{
//	std::string commandLineString = "cmd /C " + commandLine;
//
//	// output handle
//	HANDLE hOutputReadTmp, hOutputRead, hOutputWrite;
//
//	
//
//	// exit code of batch file (this is also the exit code of the python script)
//	DWORD exit_code; 
//
//	// command line: startgis "Geofile" xx.xx yy.yy
//	LPSTR commandLine_lpstr = const_cast<char *>(commandLineString.c_str());
//
//	STARTUPINFOA si;
//	PROCESS_INFORMATION pi;
//
//	ZeroMemory(&si, sizeof(si));
//	si.cb = sizeof(si);
//	ZeroMemory(&pi, sizeof(pi));
//
//	if (!CreateProcessA(NULL,
//		commandLine_lpstr,		// command line 
//		NULL,					// process security attributes 
//		NULL,					// primary thread security attributes 
//		FALSE,					// handles are inherited 
//		0,						// creation flags 
//		NULL,					// use parent's environment 
//		NULL,					// use parent's current directory 
//		&si,					// STARTUPINFO pointer 
//		&pi)					// receives PROCESS_INFORMATION 
//		)
//	{
//		printf("CreateProcess failed (%d)\n", GetLastError());
//		return FALSE;
//	}
//	WaitForSingleObject(pi.hProcess, INFINITE);
//	
//	// get exit code and save to variable exit_code
//	GetExitCodeProcess(pi.hProcess, &exit_code);
//
//	std::cout << "Exit code of python script: " << exit_code << std::endl;
//
//	// coverage
//	float coverage = 1.0f * exit_code / 100;
//
//	// close process and thread handles
//	CloseHandle(pi.hProcess);
//	CloseHandle(pi.hThread);
//
//	return coverage;
//}