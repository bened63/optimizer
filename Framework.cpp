/*
*
* (c) 2016 Airbus Defence and Space
*
****************************************************************************
*
* Responsible Persons: Benjamin Eder / Daniel Kallfass / Jakob Bauer
*
****************************************************************************
*
*/

#pragma once
#include "OptimizerUnit/Framework.h"

// ECF
#include <ecf/ECF.h>
#include "OptimizerUnit/FunctionMinEvalOp.h"
#include "OptimizerUnit/WriteResultsOp.h"
#include "OptimizerUnit/ECFOptimizerState.h"

#include <regex>

// constructor
Framework::Framework(QObject *parent) : QObject(parent)
{
}

// destructor
Framework::~Framework()
{
}

void Framework::initialize(DataFarming::ExecutionService* service, DataFarming::DataFarmingExperimentFile* dfe)
{
	// initialize execution service and data farming experiment
	_executionService = service;
	_dfe = dfe;

	// read input parameters from dfe and save in vector variable
	readParameters();
}

/**
 * read parameters from DFE and prepare data for transfer to framework
 */
bool Framework::readParameters()
{
	std::cout << "seed(dfe) = " << _dfe->getDataFarmingExperiment()->getExperimentalSeed() << std::endl; // seed from dfe

	DataFarming::ExperimentalDesign& doe = _dfe->getDataFarmingExperiment()->getExperimentalDesign(); // get DoE from DFE object
	DataFarming::ExperimentalDesign::Subdesigns& subdesigns = doe.getSubdesigns(); // get all sub designs from DoE

	// properties
	// check if single run timeout property is defined
	if (!_dfe->getDataFarmingExperiment()->getPropertyValue("singleRunTimeout").empty())
	{
		std::string srt = _dfe->getDataFarmingExperiment()->getPropertyValue("singleRunTimeout").toStdString();
		_dfe->getDataFarmingExperiment()->setSingleRunTimeOutSec(stoi(srt));
		//_properties.push_back( &srt );
	}

	// find subdesign with type OPTIMIZER
	for (Core::ref_ptr<DataFarming::Subdesign>& subdesign : subdesigns)
	{
		// only select OPTIMIZER designs
		if (!subdesign->isDesignType(DataFarming::OPTIMIZER)) { continue; }

		// get all parameter variations
		DataFarming::Subdesign::ParameterVariations& pvs = subdesign->getParameterVariations();

		// iterate over all parameter variations and create variables for ECF (framework)
		for (Core::ref_ptr<DataFarming::ParameterVariation>& pv : pvs)
		{
			assert(pv->getNumOfValues() == 1 && "Invalid number of variation values!");

			// all parameter inside a parameter variation have the same data type and ranges (if set)
			assert(pv->getNumOfParameters() > 0 && "DoE is invalid!");

			// parameter
			DataFarming::Parameter* p = pv->getParameter(0);

			//
			// type: ENUMERATION 
			//
			if (p->isType(DataFarming::ENUMERATION))
			{
				// categorical variable
				if (!p->getEnumValues().empty())
				{
					EnumerationStruct enumerationStruct;

					// varation name
					enumerationStruct.variationName = pv->getVariationName();

					// parameter name
					enumerationStruct.parameterName = p->getName();

					// string value
					enumerationStruct.stringValue = pv->getStringValues()[0];

					// type
					enumerationStruct.type = parseEnumValues(p->getEnumValues());

					// enumeration values
					for (int i = 0; i < p->getEnumValues().size(); i++)
					{
						enumerationStruct.enumValues.push_back(p->getEnumValues()[i]);
					}

					// add parameter variation to ParameterVariationStruct
					_parameterVariationStruct.enumerations.push_back(enumerationStruct);
				}
				else
				{
					// do nothing
				}

			}

			//
			// type: NUMBER
			//
			if (p->isType(DataFarming::NUMBER))
			{
				// lower and upper bound of variable
				Core::Patterns::DoubleOptional minValue = p->getMin();
				Core::Patterns::DoubleOptional maxValue = p->getMax();
				assert(minValue.isSet() && maxValue.isSet() && "DoE is invalid!");

				// for this test implementation this value is not used!
				Core::Patterns::IntOptional decimalPlaces = p->getDecimalPlaces();

				assert(pv->getDoubleValues().size() == 1 && "DoE is invalid");

				// varation name
				_numberStruct.variationName = pv->getVariationName();

				// parameter name
				_numberStruct.parameterName = p->getName();

				// double value
				_numberStruct.doubleValue = pv->getDoubleValues()[0];

				// minimum / lower bound
				_numberStruct.min = minValue();

				// maximum / upper bound
				_numberStruct.max = maxValue();

				// add parameter variation to ParameterVariationStruct
				_parameterVariationStruct.numbers.push_back(_numberStruct);
			}
		}
	}

	assert(_parameterVariationStruct.numbers.size() != 0 || _parameterVariationStruct.enumerations.size() != 0 && "Could not read type NUMBER type from dfe and add to struct.");

	return true;
}

/**
 * parse enumeration values
 * return enumeration type: "enumeration" (default) OR "vector" (special case for RiKoV)
 */
std::string Framework::parseEnumValues(DataFarming::Parameter::EnumValues& eVal)
{
	std::string type = "enumeration";

	// special case only if 2 enumeration values (min and max) with format (x,y,z)
	if (eVal.size() == 2)
	{
		// temporary regex string
		std::string str_regex = eVal.at(1); // check first element

		std::smatch sm; // matches (regex)
		std::regex regex(R"(\((-?[\.\s\d]+),(-?[\.\s\d]+),(-?[\.\s\d]+)\))"); // regex match (11,222,3333), (-2,-8,-23), (54.32, 5.42 , 43.22), ...

		// search output string for coverage value (regex)
		if (std::regex_search(str_regex, sm, regex))
		{
			//std::string x = sm.str(1);
			//std::string y = sm.str(2);
			//std::string z = sm.str(3);

			type = "vector";
		}
	}

	return type;
}

/**
 * run optimization
 * methode conforms modified main in ECF
 */
void Framework::run()
{
	// own state function (inherit from State)
	std::shared_ptr<ECFOptimizerState> state(new ECFOptimizerState); //StateP state( new State );

	// Fitness function object
	FunctionMinEvalOp* fitnessFunction = new FunctionMinEvalOp(_executionService, _dfe, _parameterVariationStruct);

	// Write results each generation (during runtime)
	// dimension must be 1 (TODO: multiple dimensions)
	WriteResultsOpP writeOp = (WriteResultsOpP) new WriteResultsOp(_executionService->getName());
	state->addOperator(writeOp);

	// set the evaluation operator
	state->setEvalOp(fitnessFunction);

	// set parameter variations in State, so they can be used to generate the genotype
	state->setGenotypeVariables(_parameterVariationStruct);

	// *no arguments in initialize anymore/at the moment*
	state->initialize();

	//state->run();

	bool bFirstTime = true;

	while (!state->isRunTerminated()) {
		//get current generation (for testing purposes)
		state->getCurrentPopulation();

		if (bFirstTime) {
			//run the first generation
			state->runInitialize();

			bFirstTime = false;
		}
		else {
			//run the next generation
			state->runNextGeneration();
		}
	}

	// end
	state->outputHallOfFame();

	//for (unsigned int i = 0; i < state->getGenerationNo(); ++i) {
	//  state->createNextGeneration();
	//  NewGeneration = state->getGeneration();
	//  dfExperimentResults = createNextDFE(newGeneration);
	//  fitnessEvalOp->setResults(dfExperimentResults);
	//  state->getEvaluate();
	//}


}