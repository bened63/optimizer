#include <ecf/ECF.h>
#include "OptimizerUnit/WriteResultsOp.h"

WriteResultsOp::WriteResultsOp(std::string applicationName)
{
	_applicationName = applicationName;
}

bool WriteResultsOp::initialize(StateP state)
{
	// file path
	_filepath = R"(data\DataFarming\)" + _applicationName + R"(\results\)";

	// file names
	_filename_coordinates = R"(coordinates.csv)";
	_filename_fitness = R"(fitness.csv)";

	// delimiter for csv file
	_delimiter = ";";

	// population size
	_populationSize = state->getPopulation()->getLocalDeme()->getSize();

	// population stats
	_popStats = state->getStats();

	// header
	// create new files, overwrite old (if existing)
	_stream_coordinates.open(_filepath + _filename_coordinates);
	_stream_coordinates << "generation" << "\n"; // head
	_stream_coordinates.close();

	_stream_fitness.open(_filepath + _filename_fitness);
	_stream_fitness << "algorithm" << _delimiter << state->getAlgorithm()->getName() << std::endl; // algorithm name
	_stream_fitness <<
		"generation" << _delimiter <<
		"min fitness" << _delimiter <<
		"max fitness" << _delimiter <<
		"average fitness" << _delimiter <<
		"deviation" << _delimiter <<
		"sample size" << _delimiter <<
		"time" << _delimiter <<
		"lowest recorded fitness" << _delimiter <<
		"highest recorded fitness" <<
		"\n"; // head
	_stream_fitness.close();

	return true;
}

/**
 * write results to file
 *
 * mandatory: actual operation (invoked at the end of each generation)
 */
bool WriteResultsOp::operate(StateP state)
{
	//
	// coordinates (FloatingPoints)
	//

	// initialize result string
	std::string result_floats = "";

	// current generation number
	uint generationNumber = state->getGenerationNo();

	// we only use one deme (here: is equal to population)
	DemeP deme = state->getPopulation()->at(0);

	// parse inividuals
	// each individual represents one solution (here: multiple FloatingPoint values)
	for (int i = 0; i < _populationSize; i++)
	{
		// get individual
		IndividualP individual = deme->at(i);

		// individual size == number of genotype values
		uint individualSize = individual->size();

		// generation number
		result_floats += std::to_string(generationNumber) + _delimiter;

		// parse each genotype value (FloatingPoint)
		for (int k = 0; k < individualSize; k++)
		{
			if (individual->getGenotype(k)->getName() == "FloatingPoint")	{
				double value = ((FloatingPoint::FloatingPoint*)individual->getGenotype(k).get())->realValue[0];

				result_floats += std::to_string(value) + _delimiter;
			}

			if (individual->getGenotype(k)->getName() == "BitString") {
				BitString::BitString* bitString = ((BitString::BitString*)individual->getGenotype(k).get());

				std::string bits = "";

				for (int ibs = 0; ibs < bitString->bits.size(); ibs++) {
					bits += std::to_string(((BitString::BitString*)individual->getGenotype(k).get())->bits[ibs]);
				}

				result_floats += bits + _delimiter;
			}

			if (individual->getGenotype(k)->getName() == "Binary") {
				double binaryVal = ((Binary::Binary*)individual->getGenotype(k).get())->realValue[0];

				result_floats += std::to_string(binaryVal) + _delimiter;
			}

		}

		// fitness
		result_floats += std::to_string(individual->fitness->getValue()) + "\n";
	}

	// write to file
	_stream_coordinates.open(_filepath + _filename_coordinates, ios::app);
	_stream_coordinates << result_floats;
	_stream_coordinates.close();

	//
	// fitness
	//

	// initialize result string
	std::string result_fitness = "";

	// vector<double> generationStats contains:
	//
	// 0: minimum fitness values for generation x
	// 1: maximum fitness values for generation x
	// 2: average fitness values for  generation x
	// 3: deviation of fitness values for generation x
	// 4: sample size / individuals size / number of genotypes/individuals
	// 5: time
	// 6: total number of evaluations for generation x
	// 7: lowest recorded fitness value
	// 8: highest recorded fitness value
	std::vector<double> generationStats = state->getPopulation()->getStats()->getStats(generationNumber);

	// generation number
	result_fitness += std::to_string(generationNumber) + _delimiter;

	// minimum fitness
	result_fitness += std::to_string(generationStats[0]) + _delimiter;

	// maximum fitness
	result_fitness += std::to_string(generationStats[1]) + _delimiter;

	// average fitness
	result_fitness += std::to_string(generationStats[2]) + _delimiter;

	// deviation
	result_fitness += std::to_string(generationStats[3]) + _delimiter;

	// sample size
	result_fitness += std::to_string(generationStats[4]) + _delimiter;

	// time
	result_fitness += std::to_string(generationStats[5]) + _delimiter;

	// lowest recorded fitness value
	result_fitness += std::to_string(generationStats[7]) + _delimiter;

	// highest recorded fitness value
	result_fitness += std::to_string(generationStats[8]) + _delimiter;

	// new line
	result_fitness += "\n";

	_stream_fitness.open(_filepath + _filename_fitness, ios::app);
	_stream_fitness << result_fitness;
	_stream_fitness.close();

	return true;
}