/*
*
* (c) 2016 Airbus Defence and Space
*
****************************************************************************
*
* Responsible Persons: Jakob Bauer / Benjamin Eder
*
****************************************************************************
*
*/
#include "OptimizerUnit/Setup.h"
#include "Core/FilenameSubstitutions.h"
#include "Core/FileSystem.h"
#include "DataFarming/ExecutionServiceCache.h"
#include "DataFarming/DataFarmingExperimentFile.h"

void setup( int argc, char *argv[] )
{
	
}

void shoutDfePath(std::string dfe) {
	std::cout <<
		"+------------------------------------------------------------------\n"
		"|\n"
		"|         ____  ____________   \n"
		"|        / __ \\/ ____/ ____/  \n"
		"|       / / / / /_  / __/      \n"
		"|      / /_/ / __/ / /___      \n"
		"|     /_____/_/   /_____/      \n"
		"|\n"
		"|\n"
		"| " << dfe << "\n"
		"|\n"
		"+------------------------------------------------------------------\n"
		<< std::endl;
}