#include "ECFOptimizerAlgorithm.h"

/**
 * overwrite parent function
 * first: execute DF experiment
 * then: eveluate results of DF experiment (callback)
 */
bool ECFOptimizerAlgorithm::initializePopulation(StateP state)
{
	std::cout << std::endl;

	for (uint iDeme = 0; iDeme < state->getPopulation()->size(); iDeme++)
		for (uint iInd = 0; iInd < state->getPopulation()->at(iDeme)->size(); iInd++)
		{
			// start DF experiment, then evaluate
			//evalOp_->setOptimizerDesign(floatingPoints);
			//executeExperiment(*_executionService);

			// evaluate muss dann von Callback Funktion aufgerufen werden
			evaluate(state->getPopulation()->at(iDeme)->at(iInd));
		}
	return true;
}