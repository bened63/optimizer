/*
*
* (c) 2016 Airbus Defence and Space
*
****************************************************************************
*
* Responsible Persons: Jakob Bauer / Benjamin Eder
*
****************************************************************************
*
*/
#pragma once
#include <QtCore/QCoreApplication>

#include "Core/FilenameSubstitutions.h"
#include "Core/FileSystem.h"

#include "DataFarming/ExecutionServiceCache.h"
#include "DataFarming/DataFarmingExperimentFile.h"

#include "OptimizerUnit/RandomOptimizer.h"
//#include "OptimizerUnit/OptimizerController.h"
#include "OptimizerUnit/Optimizer.h"
#include "OptimizerUnit/Setup.h"

// Evolutionary Computation Framework
#include "OptimizerUnit/Framework.h"

#include <regex>

/**
 * read config.ini and save values
 */
bool readConfig(std::map<std::string, std::string>& config)
{
	// stream
	std::ifstream ifsConfig("configOptimizer.ini", std::ifstream::in);

	if (!ifsConfig.is_open()) {
		std::cout << "ERROR: Could not open config.ini!" << std::endl;
		return false;
	}

	// line
	std::string line;

	// read lines
	while (std::getline(ifsConfig, line))
	{
		// temporary regex string
		std::string str_regex = line;

		std::smatch sm; // matches (regex)
		std::regex regex(R"((.+?)=(.+))"); // regex match ...=...

		// search output string for coverage value (regex)
		if (std::regex_search(str_regex, sm, regex))
		{
			//std::string x = sm.str(1);
			config[sm.str(1)] = sm.str(2);
		}
	}

	return true;
}

int main( int argc, char *argv[] )
{
	// some basic settings for data farming process
	//setup( argc, argv );

	// update environment
	Core::Tools::FilenameSubstitutions::update(argv[0]);

	// add PAXSEM bin to the search path from windows (DLLs) and own for file search algorithm
	Core::String pathValue = Core::getEnvironmentValue("PATH");
	pathValue.append("../../PAXSEM/bin", ";");
	Core::IO::FileSystem::get()->convertPathSeperators(pathValue);

	Core::setEnvironmentValue("PATH", pathValue);
	Core::setValue("Directory.SearchPath", pathValue);

	// get config
	std::map<std::string, std::string> config;

	if (!readConfig(config))
	{
		return EXIT_FAILURE;
	}
	
	// core application instance is required for Qt objects
	QCoreApplication a( argc, argv );

	// get instance of the execution service
	//		apps available:					OptimizerTestApplication || CoverageApplication || TestFunctionsApplication
	//		configuration file, e.g.:		data\DataFarming\ExecutionService\OptimizerTestApplication.xml
	//		results directory, e.g.:		data\DataFarming\OptimizerTestApplication\results
	//		application directory, e.g.:	data\DataFarming\OptimizerTestApplication
	Core::ref_ptr<DataFarming::ExecutionService> executionService = DataFarming::ExecutionServiceCache::getInstance()->getOrCreate( config["executionservice"] );
	if (!executionService.valid())
	{
		return EXIT_FAILURE;
	}

	// if true: use always default dfe (name of application)
	bool useDefaultDfe = false;

	// read DFE with optimizer design (e.g. MinDistanceOptimizer.dfe or PositionOptimization.dfe)
	//		OptimizerTestApplication:	OptimizerTestApplication.def (default)	|| MinDistanceOptimizer.dfe || category.dfe		|| categories_test.dfe
	//		CoverageApplication:		CoverageApplication.dfe (default)		|| PositionOptimization.dfe || 1_sensor.dfe    	|| 3_sensors_status.dfe  || CoverageApplication2Sensors.dfe || CoverageApplication2Sensors_v3_onOffSwitch || border_3s_no_switch_161109.dfe
	//		TestFunctionsApplication:	TestFunctionsApplication.dfe (default)	|| floats_1_sinus.dfe		|| floats_2.dfe		|| floats_4.dfe			 || floats_6.dfe
	// - search the DFE file
	Core::String dfeFilePath;
	if (!useDefaultDfe) { dfeFilePath = Core::Tools::findFileInPath(R"(data\DataFarming\)" + executionService->getName() + R"(\)" + config["dfe"]); }
	if (useDefaultDfe || dfeFilePath.empty()) { dfeFilePath = Core::Tools::findFileInPath(R"(data\DataFarming\)" + executionService->getName() + R"(\)" + executionService->getName() + R"(.dfe)"); }
	shoutDfePath(dfeFilePath);

	if (dfeFilePath.empty())
	{
		// DFE not found
		return EXIT_FAILURE;
	}

	// - load
	Core::ref_ptr<DataFarming::DataFarmingExperimentFile> dfe = DataFarming::DataFarmingExperimentFile::loadFromFile( dfeFilePath );
	if (!dfe.valid())
	{
		// DFE cannot be load
		return EXIT_FAILURE;
	}

	///////////////////////////////////////////////////////////////////////////////////////

	// evolutionary computation framework
	// the constructor is similar to the main of an example of the ecf
	Framework* ecf = new Framework(&a);
	ecf->initialize(executionService, dfe);
	ecf->run();

	// needed for close operation
	//QObject::connect(optimizer, &Optimizer::finished, &a, &QCoreApplication::quit);
	QObject::connect(ecf, &Framework::finished, &a, &QCoreApplication::quit);

	return a.exec();
}
