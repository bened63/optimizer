/*
*
* (c) 2016 Airbus Defence and Space
*
****************************************************************************
*
* Responsible Persons: Jakob Bauer / Benjamin Eder
*
****************************************************************************
*
*/

#include "OptimizerUnit/Optimizer.h"

// constructor
Optimizer::Optimizer(QObject *parent) : QObject(parent)
{
}

// destructor
Optimizer::~Optimizer()
{
}

// initialization
void Optimizer::initialize(DataFarming::ExecutionService* service, DataFarming::DataFarmingExperimentFile* dfe, Framework* ecf)
{
	//// set execution service
	//_executionService = service;

	//// set data farming experiment
	//_dfe = dfe;

	//// evolutionary computation framework
	//_ecf = ecf;

	///////////////////////////
	//// get input variables //
	///////////////////////////
	//
	//// get bounds for coordinates
	//getBounds();

	//// initialize framework
	//_ecf->initialize(_executionService, _dfe);
}

void Optimizer::getBounds()
{

}

/* ****************************************
 * start optimization loop 
 * **************************************** */
void Optimizer::runOptimization()
{
	//_ecf->run();
}